/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Victor Hugo Olivetti Alvarez $
   ======================================================================== */
#include <SDL2/SDL.h>

#define global static

global int global_width = 800;
global int global_height = 600;

int
main(int Count, char *Arg[])
{

    SDL_Init(SDL_INIT_VIDEO);

    SDL_Window *window = SDL_CreateWindow("Bejeweled",
                                          SDL_WINDOWPOS_CENTERED,
                                          SDL_WINDOWPOS_CENTERED,
                                          global_width, global_height,
                                          SDL_WINDOW_SHOWN);

    SDL_Renderer *render = SDL_CreateRenderer(window, -1,
                                              SDL_RENDERER_ACCELERATED);

    SDL_SetRenderDrawColor(render, 0, 0, 0, 0);
    SDL_RenderClear(render);
    SDL_RenderPresent(render);

    SDL_Delay(1000);
    
    SDL_Quit();
    
    return(0);
}
